package Utils;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {

	static ExtentHtmlReporter reporter;
	static ExtentReports extent;
	ExtentTest test;
	public String testcaseName , testcaseDes , author , category ;
	public static String excelFileName;
	
	@BeforeSuite(groups="common")

	public void startReport() {

		reporter = new ExtentHtmlReporter("./reports/results.html");
		reporter.setAppendExisting(true);
		extent = new ExtentReports();
		extent.attachReporter(reporter);
	}

	@BeforeMethod(groups="common")
	
	public void report() {

		test = extent.createTest(testcaseName,testcaseDes);
		test.assignAuthor(author);
		test.assignCategory(category);
		
	}
	
	public void reportStep(String des , String status) {
		
		if(status.equalsIgnoreCase("pass")) {
			test.pass(des);
		}else if(status.equalsIgnoreCase("fail")) {
			test.fail(des);
		}
	}

	@AfterSuite(groups="common")

	public void stopReport() {

		extent.flush();

	}
}
