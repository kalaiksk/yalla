package testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

public class Merge extends Annotations{
	@BeforeTest(groups="common")
	public void setData() {
		testcaseName = "Create";
		testcaseDes = "To create an lead";
		author = "Kalai";
		category ="Smoke Testing";
		
	}
	//@Ignore
	@Test(groups="regression")
	public void mlead() {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		verifyTitle("Leaftaps - TestLeaf Automation Platform");
		WebElement crm=locateElement("link", "CRM/SFA");
		crm.click();
		
		WebElement Lead=locateElement("Link", "Leads");
		click(Lead);
		
		WebElement merge = locateElement("Link", "Merge Leads");
		click(merge);
		
		WebElement flead = locateElement("xpath", "(//td[@class='titleCell']//following::a)[1]");
		click(flead);
		
		switchToWindow("Find Leads");
		
		WebElement fname = locateElement("Link", "10003");
		clickwithoutsnap(fname);
		
		
		switchToWindow("Merge Leads | opentaps CRM");
		WebElement Tolead = locateElement("xpath", "//a[@id='ext-gen604']//img");
		clickwithoutsnap(Tolead);
		
		switchToWindow("Find Leads");
		WebElement fname1=locateElement("Link", "10004");
		clickwithoutsnap(fname1);
		
		switchToWindow("Merge Leads | opentaps CRM");
		WebElement merge1=locateElement("Link", "Merge");
		clickwithoutsnap(merge1);
		
		acceptAlert();
		switchToWindow("Merge Leads | opentaps CRM");
		
	WebElement ele=driver.findElementByXPath("//li[@class='errorMessage']");
		verifyPartialText(ele, ": org.ofbiz.webapp.event.EventHandlerException:");
		

		
		
	}

}
