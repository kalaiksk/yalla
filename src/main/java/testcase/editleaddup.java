package testcase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;



public class editleaddup extends Annotations {
	@BeforeTest(groups="common")
	public void setData() {
		testcaseName ="Edit Lead";
		testcaseDes = "For editing Test Lead";
		author = "kalai";
		category="Unit Testing";
					
				
	}

	@Test(groups="sanity" ,dependsOnGroups="smoke")
	public void Elead() {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		
		
		verifyTitle("Leaftaps - TestLeaf Automation Platform");
		WebElement crm=locateElement("link", "CRM/SFA");
		crm.click();

		WebElement lead = locateElement("Link", "Leads");
		click(lead);

		WebElement Mylead = locateElement("Link", "My Leads");
		click(Mylead);

		WebElement leadname = locateElement("Link","10004");
		click(leadname);	
		
		WebElement edit = locateElement("link", "Edit");
		click(edit);
		
		WebElement comp = locateElement("xpath", "//input[@class='inputBox' and @name='companyName']");
		clear(comp);
		comp.sendKeys("Infosys");
		
		WebElement update = locateElement("xpath", "(//input[@class='smallSubmit' and @name='submitButton'])[1]");
		click(update);
		
		}
	
	
}
