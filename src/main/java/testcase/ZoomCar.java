package testcase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ZoomCar {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.zoomcar.com/chennai/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver, 10);

		driver.findElementByLinkText("Start your wonderful journey").click();
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElementByXPath("//div[contains(text(),\"Nelson Manickam Road\")]"))).click();

		driver.findElementByClassName("proceed").click();

		Date date = new Date();
		DateFormat sdf = new SimpleDateFormat("dd");
		String today = sdf.format(date);
		int tomorrow = Integer.parseInt(today)+1;
		System.out.println("tomo: "+tomorrow);

		driver.findElementByXPath("//div[contains(text(),'"+tomorrow+"')]").click();
		driver.findElementByClassName("proceed").click();
		Thread.sleep(5000);
		String text = driver.findElementByXPath("//div[contains(text(),'"+tomorrow+"')]").getText();
		
		System.out.println(text.contains(Integer.toString(tomorrow))?"Start Date Verified":"StartDate Mismatch");
		driver.findElementByClassName("proceed").click();
		List<Integer> ratepro = new ArrayList<Integer>();
		List<WebElement> rate = driver.findElementsByXPath("//div[@class='price']");
		for (WebElement eachrate : rate) {
			String strrate = eachrate.getText().replaceAll("\\D", "");
			ratepro.add(Integer.parseInt(strrate));
		}
		Integer maxrate = Collections.max(ratepro);
		System.out.println(maxrate);
		System.out.println(driver.findElementByXPath("//div[contains(text(),'"+maxrate+"')]/preceding::h3[1]").getText());
		driver.findElementByXPath("//div[contains(text(),'"+maxrate+"')]/following::button[1]").click();
		driver.close();
	}
}

