package testcase;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Utils.DataLibrary;

public class Create extends Annotations{
@BeforeTest(groups="common")
	public void setData() {
		testcaseName = "Create";
		testcaseDes = "To create an lead";
		author = "Kalai";
		category ="Smoke Testing";
		//excelFileName="createdata";
		
		
		
	}
	
	@Test(groups="smoke", dataProvider="createdata")
	public void clead(String cname, String fname, String lname) {
		
		verifyTitle("Leaftaps - TestLeaf Automation Platform");
		WebElement crm=locateElement("link", "CRM/SFA");
		crm.click();
		
		WebElement eleCreateLead = locateElement("link", "Create Lead");
		click(eleCreateLead);
		
		WebElement eleCmpyName = locateElement("id", "createLeadForm_companyName");
		eleCmpyName.sendKeys(cname);
		
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		eleFirstName.sendKeys(fname);
		
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		eleLastName.sendKeys(lname);
		
		WebElement eleSource = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(eleSource, "Direct Mail");
		
		WebElement eleCreateLeadbutton = locateElement("name", "submitButton");
		click(eleCreateLeadbutton);
		
			
		}

	@DataProvider(name="createdata")
	public Object[][] fetchdata() throws IOException {
		/*Object[][] data=new Object[2][3];
		data[0][0]="TestLEaf";
		data[0][1]="Kushi";
		data[0][2]="K";
		
		data[1][0]="Infosys";
		data[1][1]="Teena";
		data[1][2]="S";*/
		
		return DataLibrary.readExcelData();
	
}
}