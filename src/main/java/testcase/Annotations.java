package testcase;

import org.testng.annotations.Test;


import Com.yalla.selenium.api.base.SeleniumBase1;
import Com.yalla.selenium.api.design.Browser;
import Com.yalla.selenium.api.design.Element;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Annotations extends SeleniumBase1{

@Parameters({"url","username","password"})
@BeforeMethod(groups="common")

	public void login(String url, String username, String password) {
		startApp("chrome",url);
		WebElement eleUserName = locateElement("id", "username");
		clearAndType(eleUserName, username);
		WebElement elePassword = locateElement("id", "password");
		clearAndType(elePassword, password);
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);

	}

	@AfterMethod(groups="common")
	public void logout() {
		close();
	}

	@BeforeClass
	public void beforeClass() {
	}

	@AfterClass
	public void afterClass() {
	}

	@BeforeTest
	public void beforeTest() {
	}

	@AfterTest
	public void afterTest() {
	}

	@BeforeSuite
	public void beforeSuite() {
	}

	@AfterSuite
	public void afterSuite() {
	}

}
