package Com.yalla.selenium.api.base;



import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;


import org.apache.commons.io.FileUtils;
import org.apache.commons.math3.distribution.WeibullDistribution;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Com.yalla.selenium.api.design.Browser;
import Com.yalla.selenium.api.design.Element;


import net.bytebuddy.implementation.bytecode.Throw;
import Utils.Reports;

public class SeleniumBase1 extends Reports implements Browser, Element {

	public RemoteWebDriver driver;
	WebDriverWait wait;
	int i = 1;

	@Override
	public void click(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			reportStep("The Element " + ele + " clicked", "pass");
		} catch (StaleElementReferenceException e) {
			reportStep("The Element " + ele + " could not be clicked", "fail");
			//System.err.println("The Element " + ele + " could not be clicked");
			throw new RuntimeException();
		} finally {
			takeSnap();
		}

	}

	@Override
	public void append(WebElement ele, String data) {

		try {
			String text = ele.getText();
			String appendtext = text.concat(data);
			ele.clear();
			ele.sendKeys(appendtext);
			reportStep("Append text success" , "pass");
		} catch (Exception e) {
			reportStep("Append text Fail" , "fail");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public void clear(WebElement ele) {

		try {
			ele.clear();
		} catch (Exception e) {
			
			System.err.println("The Element " + ele + " is not Interactable");
			throw new RuntimeException();
		}

		finally {
			takeSnap();
		}
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			reportStep("The Data :" + data +" entered Successfully", "pass");
			//System.out.println("The Data :" + data + " entered Successfully");
		} catch (ElementNotInteractableException e) {
			reportStep("The Element " + ele + " is not Interactable", "fail");
			//System.err.println("The Element " + ele + " is not Interactable");
			throw new RuntimeException();
		} finally {
			takeSnap();
		}

	}

	@Override
	public String getElementText(WebElement ele) {
		return ele.getText();
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		return ele.getCssValue("background-color");
	}

	@Override
	public String getTypedText(WebElement ele) {
		return ele.getText();
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {

		try {
			Select dp = new Select(ele);
			dp.selectByVisibleText(value);
			reportStep("The visibletext of :" + value + " entered Successfully", "pass");
			//System.out.println("The visibletext of :" + value + " entered Successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("In Dropdown " + value + " is not available", "fail");
			//System.out.println("In Dropdown " + value + " is not available");
			throw new RuntimeException();
		} finally {
			takeSnap();
		}
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {

		try {
			Select dp = new Select(ele);
			dp.selectByIndex(index);
			reportStep("The value of index :" + index + " entered Successfully", "pass");
			//System.out.println("The value of index :" + index + " entered Successfully");
		} catch (Exception e) {
			
			reportStep("In Dropdown " + index + " is not available", "fail");
			System.err.println("In Dropdown " + index + " is not available");
			throw new RuntimeException();
		} finally {
			takeSnap();
		}
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {

		try {
			Select dp = new Select(ele);
			dp.selectByValue(value);
			reportStep("The value is :" + value + " entered Successfully","pass");
			//System.out.println("The value is :" + value + " entered Successfully");
		} catch (Exception e) {
			reportStep("In Dropdown " + value + " is not available","fail");
			//System.err.println("In Dropdown " + value + " is not available");
			throw new RuntimeException();
		} finally {
			takeSnap();
		}
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {

		String text = ele.getText();
		if(text.equalsIgnoreCase(expectedText)) {
			reportStep("Matches the Expected Text", "pass");
			return true;
		}else {
			reportStep("Not Matches the Expected Text", "fail");
		return false;
	}
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if(text.contains(expectedText)) {
			reportStep("Matches the Expected Text", "pass");
			return true;
		}else {
			reportStep("Not Matches the Expected Text", "fail");
		return false;
	}
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {

		String attributevalue = ele.getAttribute(attribute);
		if (attributevalue.equals(value)) {
			reportStep("Attribute value matches" + value, "pass");
			return true;
		}

		else {
			reportStep("Attribute value not matches" +value, "fail");
			return false;
		}
		
	}	
	
		
	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		
		try {
			String attributevalue = ele.getAttribute(attribute);
			if (attributevalue.contains(value)) {
				reportStep("Attribute value matches" + value, "pass");
			
			}

			else {
				reportStep("Attribute value not matches" +value, "fail");
				
			}
		} catch (Exception e) {
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		
		if (ele.isDisplayed()) {
			reportStep("Element Displayed", "pass");
			return true;
		} else {
			reportStep("Element not Displayed", "fail");
			return false;
		}
		
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		if (!ele.isDisplayed()) {
			// OR
      /*WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.invisibilityOf(ele));*/
			reportStep("Element Disappears", "pass");
			return true;
		} else {
			reportStep("Element Displayed", "fail");
			return false;
		}
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {

		if(ele.isEnabled()) {
			reportStep("Element is Enabled", "pass");
			return true;
		}else {
			reportStep("Element is Not Enabled", "fail");
			return false;
		}
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		if(ele.isSelected()) {
			reportStep("Element is Selected", "pass");
			return true;
		}else {
			reportStep("Element is Not Selected", "fail");
			return false;
		}
		
	}

	@Override
	public void startApp(String url) {

		try {
			System.setProperty("webdriver.chrome.driver", "./drivers/chrome.exe");
			driver.get(url);
			driver.manage().window().maximize();
		} catch (Exception e) {
			System.err.println("The Browser Could not be Launched. Hence Failed");
			throw new RuntimeException();
		} finally {
			takeSnap();
		}
	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver_64bit.exe");
				driver = new FirefoxDriver();
			} else if (browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver", "./drivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			}
			driver.navigate().to(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.err.println("The Browser Could not be Launched. Hence Failed");
			throw new RuntimeException();
		} finally {
			takeSnap();
		}

	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch (locatorType.toLowerCase()) {
			case "id":
				return driver.findElementById(value);
			case "name":
				return driver.findElementByName(value);
			case "class":
				return driver.findElementByClassName(value);
			case "link":
				return driver.findElementByLinkText(value);
			case "xpath":
				return driver.findElementByXPath(value);
			case "partiallink" :
				return driver.findElementByPartialLinkText(value);
			case "tagname":
				return driver.findElementByTagName(value);
			case "cssselector":
				return driver.findElementByCssSelector(value);
				
		}
			reportStep("Element Found "+ locatorType, "pass");
		} catch (NoSuchElementException e) {
			System.err.println("The Element with locator:" + locatorType + " Not Found with value: " + value);
			throw new RuntimeException();
		} finally {
			takeSnap();
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {

		try {
			driver.findElementById(value);
			reportStep("Element Found with given value"+ value, "pass");
		} catch (Exception e) {
			System.err.println("The Element with locator of ID is Not Found with value: " + value);
			throw new RuntimeException();
		} finally {
			takeSnap();
		}
		return null;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {

		try {
			switch (type.toLowerCase()) {
			case "id":
				return driver.findElementsById(value);
			case "name":
				return driver.findElementsByName(value);
			case "class":
				return driver.findElementsByClassName(value);
			case "link":
				return driver.findElementsByLinkText(value);
			case "xpath":
				return driver.findElementsByXPath(value);
			case "partiallink" :
				return driver.findElementsByPartialLinkText(value);
			case "tagname":
				return driver.findElementsByTagName(value);
			case "cssselector":

				return driver.findElementsByCssSelector(value);
			}
			reportStep("Element Found "+ type, "pass");
		} catch (NoSuchElementException e) {
			System.err.println("The Element with locator:" + type + " Not Found with value: " + value);
			throw new RuntimeException();
		} finally {
			takeSnap();
		}

		return null;
	}

	@Override
	public void switchToAlert() {
		try {
			driver.switchTo().alert();
		} catch (Exception e) {
			System.err.println("No Such Alert Found");
		}
	}

	@Override
	public void acceptAlert() {

		try {
			driver.switchTo().alert().accept();
		} catch (Exception e) {
			System.err.println("NoAlertPresentException");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void dismissAlert() {

		try
		{
			driver.switchTo().alert().dismiss();
		} catch (Exception e) {
			System.err.println("NoAlertPresentException");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public String getAlertText() {
		try
		{
			driver.switchTo().alert().getText();
		} catch (Exception e) {
			System.err.println("NoAlertPresentException");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}

		return null;
	}

	@Override
	public void typeAlert(String data) {

		try
		{
			driver.switchTo().alert().sendKeys(data);
		} catch (Exception e) {
			System.err.println("NoAlertPresentException");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void switchToWindow(int index) {

		try {
			Set<String> allwindows = driver.getWindowHandles();
			List<String> eachwindow = new ArrayList<>();
			eachwindow.addAll(allwindows);
			String window = eachwindow.get(index);
			driver.switchTo().window(window);
			reportStep("Window Switched to " + window + " ", "pass");
			//System.out.println("Window Switched to " + window + "  ");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.err.println("Window NotFound");
			
			throw new RuntimeException();
		} finally {
			takeSnap();
		}

	}

	@Override
	public void switchToWindow(String title) {

		try {
			Set<String> allwindows = driver.getWindowHandles();
			// List<String> eachwindow = new ArrayList<>();
			// eachwindow.addAll(allwindows);
			for (String eachwindow : allwindows) {
				driver.switchTo().window(eachwindow);
				if (driver.getTitle().equalsIgnoreCase(title)) {
					reportStep("Window Switched to " + title + " page ", "pass");
					break;
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.err.println("Window NotFound");
			throw new RuntimeException();
		} finally {
			takeSnap();
		}
	}

	@Override
	public void switchToFrame(int index) {

		try
		{
			driver.switchTo().frame(index);
		} catch (Exception e) {
			System.err.println("NoSuchFrameException");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
		} catch (Exception e) {
			System.err.println("NoSuchFrameException");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void switchToFrame(String idOrName) {
		try {
			driver.switchTo().frame(idOrName);
		} catch (Exception e) {
			System.err.println("NoSuchFrameException");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
	}

	@Override
	public void defaultContent() {
		try {
			driver.switchTo().defaultContent();
		} catch (Exception e) {
			System.err.println("NoSuchFrameException");
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public boolean verifyUrl(String url) {

		String currentUrl = driver.getCurrentUrl();
		if(currentUrl.equalsIgnoreCase(url)) {
			reportStep("Url Matches", "pass");
			return true;
		}else {
			reportStep("Url Not Matches", "fail");
			return false;
		}
		
	}

	@Override
	public boolean verifyTitle(String title) {

		String verifytitle = driver.getTitle();
		boolean titleval = verifytitle.equalsIgnoreCase(title);
		if(titleval == true) {
			reportStep("Title Matches", "pass");
			//System.out.println("Title Matches : "+title);
			return true;
		}else {
			System.err.println("Title doesn't match with given title : "+title);
			return false;
		}


		/*if(verifytitle.contains(title)) {
				System.out.println("Title Matches with Entered title "+title);
			}*/


	}

	@Override
	public void takeSnap() {

		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./snaps/snap" + i + ".png");
		try {
			FileUtils.copyFile(src, des);
			
		} catch (IOException e) {

			System.err.println("Snapshot could notbe taken");
			throw new RuntimeException();

		}
		i++;
	}

	@Override
	public void close() {
		
		try {
			driver.close();
			reportStep("Current opened Browser window Closed ", "pass");
		} catch (Exception e) {
			System.err.println("Current opened Browswer window Not closed");
		}
		

	}

	@Override
	public void quit() {
		try {
			driver.quit();
			reportStep("All Browser windows Closed ", "pass");
		} catch (Exception e) {
			System.err.println("All Browser windows Not Closed");
		}
	}

	@Override
	public String getColor(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getstyle(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void clickwithoutsnap(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			reportStep("The Element " + ele + " clicked", "pass");
		} catch (StaleElementReferenceException e) {
			reportStep("The Element " + ele + " could not be clicked", "fail");
			//System.err.println("The Element " + ele + " could not be clicked");
			throw new RuntimeException();
		} finally {
			
		}
		// TODO Auto-generated method stub
		
	}

}
