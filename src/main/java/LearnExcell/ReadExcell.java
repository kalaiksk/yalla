package LearnExcell;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import cucumber.api.java.sl.Ce;

public class ReadExcell {

	public static void main(String[] args) throws IOException {
		
		XSSFWorkbook wbook=new XSSFWorkbook("./data/createdata.xlsx");
		XSSFSheet sheet=wbook.getSheetAt(0);
		int rowcount=sheet.getLastRowNum();
		System.out.println("Rowcount is" + rowcount);
		
		int colcount =sheet.getRow(0).getLastCellNum();	
		System.out.println("Colcount is" + colcount);
		
		//for rows
		for(int i=1;i<=rowcount;i++) {
			XSSFRow row =sheet.getRow(i);
			
			//for column
			for(int j=0;j<colcount;j++) {
				XSSFCell cell=row.getCell(j);
				
				String Stringcellvalue=cell.getStringCellValue();
				System.out.println(Stringcellvalue);
			}
			
		}
		
	}

}
